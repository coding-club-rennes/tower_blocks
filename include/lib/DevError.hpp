//
// DevError.hpp for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
// 
// Made by bertho_d
// Login   <bertho_d@epitech.net>
// 
// Started on  Sun Aug  3 23:35:22 2014 bertho_d
// Last update Sun Aug  3 23:46:34 2014 bertho_d
//

#ifndef DEVERROR_HPP_
# define DEVERROR_HPP_

# include "Error.hpp"

enum			t_devErrCode
{
  NO_SDL_CONTEXT
};

class			DevError : public Error
{
public:
  DevError(t_devErrCode errcode);

protected:
  t_devErrCode		_errcode;

private:
  static const char	*devErrorMessages[];
};

#endif
