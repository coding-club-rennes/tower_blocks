//
// FileError.cpp for minecraft++ in /home/thepatriot/thepatriotsrepo/minecraft++
// 
// Made by bertho_d
// Login   <bertho_d@epitech.net>
// 
// Started on  Sun Aug  3 00:00:52 2014 bertho_d
// Last update Tue Aug  5 20:54:29 2014 bertho_d
//

#include "FileError.hpp"

const char	*FileError::fileErrorMessages[] = {
  "Can't access file",
  "Can't read file",
  "Invalid GLSL Shader file",
  "Failed to link GL program"
};

FileError::FileError(t_fileErrCode errcode, const char *filename) : _errcode(errcode)
{
  if (filename)
    {
      this->_message += filename;
      this->_message += ": ";
    }
  this->_message += FileError::fileErrorMessages[errcode];
}
